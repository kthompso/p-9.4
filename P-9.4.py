'''Project P-9.4 Search Engine'''

import kindex, sys, getopt, collections, threading, time, math, urllib.request, concurrent.futures
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs

index = kindex.Index()
urlqueue = collections.deque()

#For tweaking search results
HIT_VAL = 1
TITLE_VAL = 20
URL_VAL = 50

# TODO Need to rank based on title, URL, and also authority.  For now authority
# can just be incoming links or incoming vs outgoing.  To that end, need to start storing
# the in-links in addition to out-links.

def rank(searchResults, searchterms):
    '''Ranks search results in terms of total hits for all words'''
    print("Rank: ", searchResults)
    ranks = {}
    rv = []
    for pageId in searchResults:
        rank = 0
        info = index.getInfo(pageId)
        if info == None:
            print("Error:  Couldn't find url in index (" + str(pageId) + ")")
            continue
        hits = 0
        intitle = 0
        inurl = 0
        print("SR:", searchResults)
        for term in searchterms:
            term = term.lower()
            #TODO maybe this stuff should be counted in terms of total words.
            hits = index.getHits(info.id, term) * HIT_VAL
            intitle = info.title.lower().count(term) * TITLE_VAL
            inurl = info.url.lower().count(term) * URL_VAL

        rank = (intitle + inurl + hits) * info.rank
        rv.append((pageId, rank))

    #TODO should really make the ranking into a list comprehension not this loop
    print("Search Results:")
    for elem in rv:
        pinfo = index.getInfo(elem[0])
        print(pinfo.id, pinfo.url, pinfo.rank)
    rv = [i[0] for i in sorted(rv, key=lambda rank: rank[1])]
    return rv

#Credit: https://docs.python.org/3/library/concurrent.futures.html#threadpoolexecutor-example
# Retrieve a single page and report the url and contents
def load_url(url, timeout, baseurl):
    #Only indexes pages which start with the base url
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        data = conn.read()
        info = index.indexPage(url, data)
        if info == None:
            return
        for url in info.outlinks:
            if url not in urlqueue and url.startswith(baseurl) and not index.is_indexed(url):
                urlqueue.append(url)

class WebSpider(threading.Thread):
    def __init__(self, url, maxindex):
        self.url = url
        urlqueue.append(url)
        self.maxindex = maxindex
    def run(self):
        with concurrent.futures.ThreadPoolExecutor() as executor:
            index_counter = 0
            while maxindex < 0 or index_counter < self.maxindex:
                print("Index Queue Size: ", len(urlqueue))
                pops = 0
                while urlqueue and pops < 10:
                    url = urlqueue.popleft()
                    if index.is_indexed(url):
                        continue
                    executor.submit(load_url, url, 30, self.url)
                    pops += 1
                time.sleep(1)
                index_counter += 1
                if index_counter > 5:
                    index.runSurferSimulation()
                    index_counter = 0
        print("## Done indexing ##")

class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type','text/html')
        self.end_headers()

        # Send message back to client

        o = urlparse(self.path)
        query = parse_qs(o.query)
        if 'search' in query:
            print("Query: ", query)
            terms = query['search']
            searchterms = ' '.join(terms) 
            print("Search terms:", searchterms)
            message = '<h1>Search Results:' + searchterms + '</h1><br/></br>'
            sr = search(searchterms)
            rankedResults = rank(sr, searchterms)
            if len(rankedResults) > 0:
                #url = index.getUrl(pageId)
                message += ''.join(['<a href={:s}>{:s}</a><br/>'.format(index.getUrl(pageId), index.getUrl(pageId)) for pageId in rankedResults])
                '''
                for pageId in rankedResults:
                    url = index.getUrl(pageId)
                    if url != None:
                        message += '<a href=' + url + '>' + url + '</a><br/>'
                '''
            else:
                message += 'No results found for: ' + searchterms
        elif 'allinfo' in query:
            infos = index.getAllInfo()
            message = '''<h1>All Page Info</h1>
                <br/><br/>
                <table>
                <tr><td>ID</td><td>URL</td><td>Rank</td>''' + ''.join(
                    ["<tr><td>{:d}</td><td>{:s}</td><td>{:f}</td></tr>".format(
                        info.id, info.url, info.rank) 
                        for info in sorted(infos.values(), key=lambda info: info.rank, reverse=True)]) + "</table>"
        else:
            # Write content as utf-8 data
            message = 'Use the search parameter.  E.g. <a href=?search=python>?search=python</a>'
        self.wfile.write(bytes(message, "utf8"))
        return

def runServer():
    print('starting server...')
    server_address = ('127.0.0.1', 8081)
    httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
    print('running server...')
    httpd.serve_forever()

def search(terms):
    return index.search(terms)

def usage():
    print("Usage:  python searchengine.py [-h | --help] [-m ... | --maxindex=] [-u ... | -url=]")
    print("\t-h:\thelp")
    print("\t-m:\tmaximum number of pages to index before stopping indexing.")
    print("\t-u:\turl to search through.  This is the first indexed page and only pages that start with this url are indexed. Defaults to https://www.yahoo.com.")

def main(argv):
    maxindex = -1
    url = "https://www.yahoo.com"
    try:
        opts, args = getopt.getopt(argv, "hm:u:", ["help", "maxindex=", "url="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ('-m', "--maxindex"):
            maxindex = arg
        elif opt in ("-u", "--url"):
            url = arg

    spider = WebSpider(url, maxindex)
    spider.start()
    runServer()

if __name__ == '__main__':
    main(sys.argv[1:])