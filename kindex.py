import ktrie, datetime, re, random, urllib, urllib.parse, threading
from bs4 import BeautifulSoup
from itertools import groupby
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

lock = threading.Lock()

class PageInfo(object):
    def __init__(self, pageid, url):
        self.id = pageid
        self.url = url
        self.title = ''
        self.metaTags = []
        self.outlinks = set()
        self.rank = 0.0
    def outdegree(self):
        return len(self.outlinks)

#Saves all the pages and their ids
class PageIndex(object):
    def __init__(self):
        self.pageids = {} #Key is url, value is the page id
        self.pageinfos = {} #Key is page id, value is page info
        #self.lock = threading.Lock()
    def getInfo(self, pageid):
        if pageid in self.pageinfos:
            return self.pageinfos[pageid]
        return None
    def getId(self, url):
        if url in self.pageids:
            return self.pageids[url]
        return None
    def update(self, pageInfo):
        if pageInfo == None or pageInfo.id == None:
            return
        self.pageinfos[pageInfo.id] = pageInfo
        if pageInfo.url not in self.pageids:
            self.pageids[pageInfo.url] = pageInfo.id
    def isIndexed(self, url):
        return (url in self.pageids)
    def runSurferSimulation(self):
        print("runSurferSimulation")
        #with self.lock:
        if len(self.pageinfos) < 1:
            return
        #jumps should be based on the number of nodes/edges in the graph
        # OR it should keep go
        #should build a graph then use the surfer simulation
        maxSims = 20
        deltaLimit = 0.001
        prevRanks = {k: 0 for k in self.pageinfos.keys()}
        infoCopy = self.pageinfos
        for sim in range(maxSims):
            print("Running simulation #{:d}".format(sim))
            currRanks = self.runSim(prevRanks, infoCopy, len(infoCopy) * 10 * (sim + 1))
            # Calculate average difference between all the previous and current ranks
            delta = (sum([abs(currRanks[k] - prevRanks[k]) for k in infoCopy.keys()]) / len(infoCopy))
            print("Delta: {:f}".format(delta))
            if delta < deltaLimit:
                print("Delta {:f} less than deltaLimit {:f}. Setting ranks.".format(delta, deltaLimit))
                #Set all ranks, end simulation
                for info in infoCopy.values():
                    self.pageinfos[info.id].rank = currRanks[info.id]
                return
            prevRanks = currRanks
        print("Unable to come to consensus on ranks")
    def runSim(self, currRanks, allInfos, jumps=1000):
        nodes = {} #Holds node ids and their surfer score
        edges = {}
        for page in allInfos.values():
            nodes[page.id] = 0
            edges[page.id] = []
            for olink in page.outlinks:
                destId = self.getId(olink)
                if destId:
                    edges[page.id].append(destId)
        node = None
        i = 0
        while i < jumps:
            if node == None or len(edges[node]) == 0:
                #jump to a random node
                node = random.choice(list(nodes.keys()))
            else:
                #check for a random jump
                m = random.randint(1, 100)
                if m < 15:
                    #random jump
                    node = random.choice(list(nodes.keys()))
                else:
                    #jump to a random link
                    node = random.choice(edges[node])
                #otherwise pick a random link from the current node and go
            nodes[node] += 1
            i += 1
        #print("Nodes:", nodes)
        #print("Edges:", edges)
        for node in nodes.keys():
            nodes[node] /= jumps
            if currRanks[node] != 0:
                nodes[node] = (nodes[node] + currRanks[node]) / 2
            #self.getInfo(node).rank = nodes[node]
        return nodes

def getPage(url):
    try:
        return urlopen(url).read()
    except HTTPError as e:
        return e.read()

stopwords = [
    'I', 'a', 'about', 'an', 'are', 'as', 'at', 'be', 'by', 'com', 
    'for', 'from', 'how', 'in', 'is', 'it', 'of', 'on', 'or', 'that',
    'the', 'this', 'to', 'was', 'what', 'when', 'where', 'who', 'will',
    'with', 'the', 'www'
]

def normalizeUrl(url):
    if type(url) != str or len(url) <= 0:
        return url
    while url[-1] == "/":
        url = url[:-1]
    if url.find('://'):
        url = url[url.find('://'):]

class Index(object):
    def __init__(self):
        self.trie = ktrie.Trie()
        self.nextKey = 0
        self.pageIndex = PageIndex()
        self.indexingLock = threading.Lock()
    def is_indexed(self, url):
        return self.pageIndex.isIndexed(url)
    def getHits(self, pageId, word):
        hits = self.trie.search(word)
        if hits and pageId in hits:
            return len(hits[pageId])
        return 0
    def indexPage(self, url, page):
        #print("Indexing page:", url)
        #Simulation and indexing should share the same lock
        with self.indexingLock:
            if self.is_indexed(url):
                #print("Page previously indexed.  Ignoring.", url)
                return None
            pageKey = self.nextKey
            self.nextKey += 1
            info = PageInfo(pageKey, url)
            self.pageIndex.update(info)
            soup = BeautifulSoup(page, 'html.parser')
            '''
                meta = list(re.findall(r'<meta(.*?)(/)*>', html))
                if meta:
                    info.metaTags = meta
            '''
            title = soup.title.string if soup.title != None else ""
            anchors = set()
            for link in soup.find_all('a'):
                linkurl = link.get('href')
                if linkurl:
                    #TODO could be a little smarter
                    if len(linkurl) > 0 and linkurl[0] == "/":
                        linkurl = urllib.parse.urljoin(url, linkurl)
                    while linkurl[-1] == "/": #Kill trailing slash
                        linkurl = linkurl[:-1]
                    if linkurl.find('?') != -1: #Kill trailing parameters
                        linkurl = linkurl[:linkurl.find('?')]
                    anchors.add(linkurl)
            text = re.sub("[^a-zA-Z]", " ", soup.get_text())
        #with self.indexingLock:
            info.outlinks = anchors
            info.title = title
            # TODO can we do better here?
            i = 0
            for word in text.split():
                if word not in stopwords:
                    self.trie.insert(word, pageKey, i)
                i += len(word)
            self.pageIndex.update(info)
        return info
    def search(self, query):
        rv = {}
        for term in query:
            sr = self.trie.search(term)
            #print("Index Search res: ", term, sr)
            if sr == None:
                continue
            for pageid in sr.keys():
                if pageid in rv:
                    rv[pageid].append(sr[pageid])
                else:
                    rv[pageid] = sr[pageid]
        return rv
    def getUrl(self, pageId):
        info = self.pageIndex.getInfo(pageId)
        if info != None:
            return info.url
        return None
    def getInfo(self, pageId):
        return self.pageIndex.getInfo(pageId)
    def getAllInfo(self):
        #Returns a dict where key=pageid and value=pageinfo
        return self.pageIndex.pageinfos
    def printIndex(self):
        self.trie.printTrie()
    def runSurferSimulation(self):
        with self.indexingLock:
            self.pageIndex.runSurferSimulation()