'''Binary tree implementation
    Author:  Kyle Thompson-Bass'''

# Should implement this as a balanced binary tree. Redblack maybe.

class Node(object):
    def __init__(self, key, value):
        self.key = key
        self.value = value

class BiTree(object):
    def __init__(self):
        self.vector = [None]
    def insert(self, key, value):
        i = 1
        while True:
            if (len(self.vector) <= i):
                # Double the size of the vector. Must be a better way than filling with None?
                self.vector.extend([None]*(len(self.vector)))
            node = self.vector[i]
            if node == None or node.key == key:
                self.vector[i] = Node(key, value)
                break
            elif key < node.key:
                i = i * 2
            else:
                i = i * 2 + 1
    def search(self, key):
        # May need to be returning the index of the parent in the case that it
        # doesn't exist?  probably not...
        i = 1
        while i < len(self.vector):
            node = self.vector[i]
            if node == None:
                return None
            if node.key == key:
                #return node
                return i
            elif key < node.key:
                i = i * 2
            else:
                i = i * 2 + 1
        return None
    def delete(self, key):
        i = self.search(key)
        if i != None:
            self.vector[i] = None
        # What is the bubble up procedure here?  Should probably be implementing
        # a red/black or (2,4) tree not just a straight binary tree.  So need some
        # kind of operation on insertion and deletion to keep it balanced.
    def printTree(self):
        for i, node in enumerate(self.vector):
            if node == None:
                print(i, node)
            else:
                print(i, node.key, ", ", node.value)

def mytest():
    t = BiTree()
    t.insert(4, 'four')
    t.insert(3, 'three')
    t.insert(1, 'one')
    t.insert(2, 'two')
    t.printTree()

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
    mytest()