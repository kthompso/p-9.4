'''Project P-9.4 Search Engine'''

import kindex, WebSpider, sys, threading, getopt, collections, time, math, urllib.request
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs

index = kindex.Index()

#For tweaking search results
HIT_VAL = 1
TITLE_VAL = 30
URL_VAL = 100

# TODO Need to rank based on title, URL, and also authority.  For now authority
# can just be incoming links or incoming vs outgoing.  To that end, need to start storing
# the in-links in addition to out-links.

def rank(searchResults, searchterms):
    '''Ranks search results in terms of total hits for all words'''
    ranks = {}
    rv = []
    for pageId in searchResults:
        rank = 0
        info = index.getInfo(pageId)
        if info == None:
            print("Error:  Couldn't find url in index ({:s})".format(str(pageId)))
            continue
        hits = 0
        intitle = 0
        inurl = 0
        #print("SR:", searchResults)
        #print("SearchTerms:", searchterms)
        termsInTitle = 0
        termsInUrl = 0
        for term in searchterms:
            term = term.lower()
            #TODO maybe this stuff should be counted in terms of total words.
            hits += index.getHits(info.id, term)
            intitle += info.title.lower().count(term) if info.title != None else 0
            inurl += info.url.lower().count(term) if info.url != None else 0
            if intitle > 0:
                termsInTitle += 1
            if inurl > 0:
                termsInUrl += 1
            #print("UrL:", info.url.lower(), "Term:", term, "urlscore:", inurl, "In Url Count:", info.url.lower().count(term))
        rank = ((intitle * TITLE_VAL + inurl * URL_VAL + hits * HIT_VAL) * info.rank) + inurl + (0.25 * intitle)
        if termsInUrl == len(searchterms):
            rank += 3
        if termsInTitle == len(searchterms):
            rank += 1
        rv.append((pageId, rank, intitle, inurl, hits, info.rank))
    #TODO should really make the ranking into a list comprehension not this loop
    #print("Search Results:")
    #for elem in rv:
    #    pinfo = index.getInfo(elem[0])
    #    print(pinfo.id, pinfo.url, pinfo.rank)
    rv.sort(key=lambda elem: elem[1], reverse=True)
    return rv

class KHTTPServer_Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        o = urlparse(self.path)
        query = parse_qs(o.query)
        if 'search' in query:
            terms = query['search'][0].split()
            message = '<h1>Search Results:{:s}</h1><br/>'.format(' '.join(terms))
            results = rank(search(terms), terms)
            for i, r in enumerate(results):
                reason = "Reason: [ Score:{:f} | TitleHits:{:d} | UrlHits:{:d} | BodyHits:{:d} | Authority:{:f} ]".format(r[1],r[2],r[3],r[4],r[5])
                url = index.getUrl(r[0])
                #title = index.getInfo(r[0]).title
                #title = title if title else url
                title = url
                message += '<b>{:d}</b> <a href=\"{:s}\" title=\"{:s}\">{:s}</a><br/>'.format(i, url, reason, title)
            if len(results) == 0:
                message += 'No results found for:{:s}'.format(' '.join(terms))
        elif 'allinfo' in query:
            infos = index.getAllInfo()
            message = '''<h1>All Page Info</h1>
                <br/><br/>
                <table>
                <tr><td>ID</td><td>URL</td><td>Authority</td>''' + ''.join(
                    ["<tr><td>{:d}</td><td>{:s}</td><td>{:f}</td></tr>".format(
                        info.id, info.url, info.rank) 
                        for info in sorted(infos.values(), key=lambda info: info.rank, reverse=True)]) + "</table>"
        else:
            message = 'Use the search parameter.  E.g. <a href=?search=python>?search=python</a>'
        self.wfile.write(bytes(message, "utf8"))

def search(terms):
    return index.search(terms)

def usage():
    print("Usage:  python searchengine.py [-h | --help] [-m ... | --maxindex=] [-u ... | --url=] [-t ... | --threads=]")
    print("\t-h:\tDisplays this message")
    print("\t-m:\tMax # of pages to index.")
    print("\t-u:\tUrl to start with.  This is the first indexed page and only pages that start with this url are indexed. Defaults to https://www.yahoo.com.")
    print("\t-t:\tThreads.  Number of threads to use in webcrawler.")

def main(argv):
    maxindex = 1000
    url = "https://www.yahoo.com"
    threads = 4
    try:
        opts, args = getopt.getopt(argv, "hm:u:t:", ["help", "maxindex=", "url=", "threads="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ('-m', "--maxindex"):
            maxindex = int(arg)
        elif opt in ("-u", "--url"):
            url = arg
        elif opt in ("-t", "--threads"):
            threads = int(arg)
    run_event = threading.Event()
    run_event.set()
    try:
        print("Starting web spider")
        spider = WebSpider.WebSpider(url, index, maxindex, threads, run_event)
        spider.start()
        print("Web spider running")
        print('starting server...')
        server_address = ('127.0.0.1', 8081)
        httpd = HTTPServer(server_address, KHTTPServer_Handler)
        print('running server...')
        httpd.serve_forever()
    except KeyboardInterrupt:
        if run_event.is_set(): run_event.clear()
        spider.join()

if __name__ == '__main__':
    main(sys.argv[1:])