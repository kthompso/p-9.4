import time, urllib
from urllib.parse import urlparse, parse_qs
from threading import Thread, Lock
from queue import Queue

indexCounter = 0 #Keeps track of how many pages are crawled.
indexCounterLock = Lock()

#Credit: https://docs.python.org/3/library/concurrent.futures.html#threadpoolexecutor-example
# Retrieve a single page and report the url and contents
def load_url(url, index, timeout, baseurl, queue):
    #Only indexes pages which start with the base url
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        try:
            data = conn.read()
        except:
            print("Problem reading url:", url)
            return
        info = index.indexPage(url, data)
        if info == None:
            return False
        for url in info.outlinks:
            if url.startswith(baseurl) and not index.is_indexed(url):
                queue.put(url)
    return True

def indexUrls(baseurl, queue, index, run_event, maxindex):
    global indexCounter
    while run_event.is_set():
        if load_url(queue.get(), index, 30, baseurl, queue):
            with indexCounterLock:
                if indexCounter > maxindex:
                    break
                indexCounter += 1
        queue.task_done()

class WebSpider(Thread):
    def __init__(self, url, index, maxindex, threads, run_event):
        super(WebSpider, self).__init__()
        self.url = url
        self.index = index
        self.maxindex = maxindex
        self.run_event = run_event
        self.threads = max(threads, 1)
    def run(self):
        queue = Queue()
        queue.put(self.url)
        workers = []
        for i in range(self.threads):
            worker = Thread(target=indexUrls, args=(self.url, queue, self.index, self.run_event,self.maxindex,))
            worker.setDaemon(True)
            worker.start()
            workers.append(worker)
        with indexCounterLock: prevIndexCount = indexCounter
        while self.run_event.is_set():
            with indexCounterLock:
                if indexCounter >= self.maxindex:
                    self.run_event.clear()
            time.sleep(20)
            runSim = False
            with indexCounterLock: #Only runs the simulation if new elements have been indexed.
                runSim = (indexCounter != prevIndexCount)
                prevIndexCount = indexCounter
            if runSim: self.index.runSurferSimulation()
        for worker in workers:
            worker.join()