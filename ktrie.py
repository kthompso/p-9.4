'''
File: ktrie.py
Desc: (Kyle Trie) Compressed trie implementation for indexing web pages
Author:  Kyle Thompson-Bass
'''

import threading

# TODO is the node based multi-way tree the best way to implement a trie? Probably not, should try to
# implement it in a more space efficient way.

class Node(object):
    # TODO store locations in a sorted sequence. Easier for searching meta/title info
    def __init__(self, s):
        self.val = s;
        self.children = {}
        self.parent = None
        self.matches = {}
    def addChild(self, s):
        node = Node(s)
        node.parent = self
        self.children[s[0]] = node
        return node
    def addLocation(self, pageKey, location):
        if pageKey in self.matches:
            self.matches[pageKey].append(location)
        else:
            self.matches[pageKey] = [location]
    def getLocations(self):
        return self.getMatches()
    def getMatches(self):
        return self.matches
    def getChildWithChar(self, c):
        if c in self.children:
            return self.children[c]
        return None
    def split(self, x, s):
        '''Splits node after index x and creates a new node under x with string s
            returns the node containing string s, or the new parent node if s is empty'''
        newNode = self.parent.addChild(self.val[:x])
        newNode.children[self.val[x]] = self
        self.parent = newNode
        newNode.parent.children[newNode.val[0]] = newNode
        self.val = self.val[x:]
        if s != '':
            return newNode.addChild(s)
        return newNode
    def getWord(self):
        s = self.val
        p = self.parent
        while p and p.val:
            s = p.val + s
            p = p.parent
        return s

class Trie(object):
    def __init__(self):
        self.root = Node(None)
    def insert(self, word, pageKey, position):
        if type(word) != str or len(word) == 0:
            return
        node = self.root
        x = i = 0
        while i < len(word):
            next = node.getChildWithChar(word[i])
            if next == None:
                node = node.addChild(word[i:])
                break
            else:
                i += 1
                x = 1
                while x < len(next.val) and i < len(word):
                    if next.val[x] == word[i]:
                        x += 1
                        i += 1
                    else:
                        next = next.split(x, word[i:])
                        i = len(word)
                        x = len(next.val)
                if i == len(word) and x < len(next.val):
                    next = next.split(x, '')
            node = next
        node.addLocation(pageKey, position)
    def search(self, pattern):
        '''Returns the indexes of matches for pattern in text
            for example:

                >>> from pprint import pprint
                >>> t = Trie()
                >>> i = 0
                >>> for word in "the dog ran through the dog do door do".split(): i += len(word); t.insert(word, 1, i); i+=1
                >>> pprint(t.search("dog"))
                {1: [7, 27]}
                >>> pprint(t.search("notdog"))
                None
                >>> pprint(t.search("door"))
                {1: [35]}
                >>> pprint(t.search(''))
                None
        '''
        if type(pattern) != str or len(pattern) <= 0:
            return None
        node = self.root
        i = 0
        while i < len(pattern):
            node = node.getChildWithChar(pattern[i])
            if node == None or (i + len(node.val)) > len(pattern) or pattern[i:i+len(node.val)] != node.val:
                return None
            i += len(node.val)
        return node.matches
    def printTrie(self, node=None, tabs=0):
        if node == None:
            node = self.root
        self.printNode(node, tabs)
        tabs += 1
        for child in node.children.values():
            self.printTrie(child, tabs)
    def printNode(self, node, tabs):
        s = ''
        for i in range(tabs):
            s += '\t'
        v = node.val
        if v == None:
            v = "Root"
        loc = node.getLocations()
        print(s, v, loc)

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False)
    '''
    t = Trie()
    i = 0
    for word in "the dog ran through the dog do door do".split(): 
        print("Insert:", word)
        i += len(word)
        t.insert(word, 1, i)
        i+=1
        t.printTrie()
    print(t.search("dog"))
    t.printTrie()
    '''