'''Splay tree implementation
    Author:  Kyle Thompson-Bass'''

import logging
log = logging.getLogger('splay')

# TODO Since this is a binary tree we can easily do this as a vector instead of
# a linked list.
class Node(object):
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.parent = None
        self.left = None
        self.right = None
    def splay(self):
        log.debug("Node.splay[%s, %s]", self.key, self.value)
        counter = 0
        while self.parent != None and counter < 10:
            counter += 1
            log.debug('Splaying: %s, %d', self.key, counter)
            if self.parent.parent == None:
                self.zig()
            elif (self.parent == self.parent.parent.right and self == self.parent.right) or (self.parent == self.parent.parent.left and self == self.parent.left):
                self.zigzig()
            else:
                self.zigzag()
    def zigzig(self):
        '''Used when current node is the first or last node in an inorder
            traversal of it's grandparent, parent, and self.'''
        log.debug('zigzig(%s)', self.key)
        p = self.parent
        gp = p.parent
        if p.right and self.key == p.right.key:
            selftmp = self.left
            ptmp = p.left
            self.left = p
            p.left = gp
            p.right = selftmp
            gp.right = ptmp
        else:
            selftmp = self.right
            ptmp = p.right
            self.right = p
            p.right = gp
            p.left = selftmp
            gp.left = ptmp
        self.parent = gp.parent
        gp.parent = p
        p.parent = self
    def zigzag(self):
        '''Used then the current node is the middle node in an inorder traversal
            of it's grandparent, parent, and self.'''
        log.debug('zigzag(%s)', self.key)
        p = self.parent
        if p == None: return
        gp = p.parent
        if gp == None: return
        tmpleft = self.left
        tmpright = self.right
        if p.left and self.key == p.left.key:
            self.left = gp
            self.right = p
            gp.right = tmpleft
            p.left = tmpright
        else:
            self.right = gp
            self.left = p
            gp.left = tmpright
            p.right = tmpleft
        p.parent = self
        self.parent = gp.parent
        gp.parent = self
    def zig(self):
        '''Used when parent is the root (no grandparent) to move current Node
            to the root position'''
        log.debug('zig(%s)', self.key)
        p = self.parent
        if p == None:
            return
        if p.right and self.key == p.right.key:
            tmp = self.left
            self.left = p
            p.right = tmp
            tmp = p.parent # should be None
            p.parent = self
            self.parent = tmp
        else:
            tmp = self.right
            self.right = p
            p.left = tmp
            tmp = p.parent # should be None
            p.parent = self
            self.parent = tmp
        log.debug('After zig: parent:(%s) \tgp:(%s)', self.parent.key if self.parent else None, self.parent.parent.key if self.parent and self.parent.parent else None)

class SplayTree(object):
    def __init__(self):
        self.root = None
        self.size = 0
    def insert(self, key, value):
        log.debug('SplayTree.insert(%s, %s)',  key, value)
        n = self.search(key, False)
        if n == None:
            log.debug('Tree is empty, setting to root node: (%s)', key)
            self.root = Node(key, value)
            self.size += 1
        elif n.key == key:
            log.debug('Existing key found: (%s, %s)', n.key, n.value)
            # Already exists with this key?
            n.value = value
            self.splayNode(n)
        else:
            log.debug('Key not found: (%s)', key)
            #Should be parent
            newNode = Node(key, value)
            if key < n.key:
                n.left = newNode
            else:
                n.right = newNode
            newNode.parent = n
            self.size += 1
            self.splayNode(newNode)
    def delete(self, key):
        log.debug('SplayTree.delete(%s)', key)
        n = self.search(key)
        if n == None:
            return
        else:
            # TODO
            self.size -= 1
    def search(self, key, splay=True):
        '''Finds node with key k in tree
                >>> t = SplayTree()
                >>> t.insert(5, "five")
                >>> t.insert(4, "four")
                >>> t.search(4).value
                'four'
                >>> t.search(5).value
                'five'
        '''
        log.debug('SplayTree.search(%s)', key)
        n = self.root
        if n == None or n.key == key:
            return n
        while n != None:
            if key < n.key:
                if n.left:
                    n = n.left
                else:
                    if splay:
                        self.splayNode(n)
                    return n
            elif key > n.key:
                if n.right:
                    n = n.right
                else:
                    if splay:
                        self.splayNode(n)
                    return n
            else:
                if splay:
                    self.splayNode(n)
                return n
        if n != None:
            if splay:
                self.splayNode(n)
            return n
        return None
    def splayNode(self, node):
        if node == None:
            return
        node.splay()
        self.root = node
    def printT(self, node=None, tabs=0):
        if node == None:
            node = self.root
        s = ''
        for i in range(tabs):
            s += "\t"
        if node.parent != None:
            if node.parent.left and node.parent.left.key == node.key:
                s += "L"
            else:
                s += "R"
        print(s, "[" + str(node.key) + "," + node.value + "]")
        tabs += 1
        if node.left != None:
            self.printT(node.left, tabs)
        if node.right != None:
            self.printT(node.right, tabs)

def mytest():
    import logging, sys
    logging.basicConfig(
        level = logging.INFO,
        stream = sys.stdout,
        format = "%(levelname)-10s %(asctime)s %(message)s"
    )
    t = SplayTree()
    t.insert(5, "five")
    t.printT()
    t.insert(4, "four")
    t.printT()
    t.insert(6, "six")
    t.printT()
    t.insert(7, "seven")
    for i in [5, 7, 6, 4]:
        print("Search ", i)
        print("Before:")
        t.printT()
        print("Found:", t.search(i).key)
        print("After:")
        t.printT()
        print("\t\t")

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)