#Basic Search Engine#
##Final Project 9.4##
Author:  **Kyle Thompson-Bass**

##Description##

**Problem:** Create a basic search engine.

Search engines typically have to perform two tasks.  Documents need to be **indexed** then **searched/ranked**.  Indexing is done by a web spider which crawls documents whose addresses have been extracted from other documents in a recurring process.

##Indexing##

I implemented a multi-threaded web spider which starts with a single URL, catalogs any links on that document (saving them for later crawling) and then builds a compressed trie from the text in the document.  The compressed trie (ktrie.py), keeps an occurrence list for each word, which lists all the id's of the documents containing that word and the index of every occurrence within those documents.  After the first document is indexed, moves on to other documents whose addresses it has extracted from anchor tags and so on.

The web spider benefits significantly from multi-threading because the major bottleneck in indexing documents is downloading.  So we have multiple threads who spend the majority of their execution time reading from connections, and then a small amount of time indexing those pages.  *Writing* to the index is thread-safe and only allows one indexing operation at a time (Also, running the surfer simulation postpones any indexing, to stop the graph from changing.  More on that later.).  *Reading* from the index is not thread safe, but it's not really an issue (at least not for this application).

To parse the HTML documents, I utilized the BeautifulSoup package.  This choice was made because parsing HTML effectively is extremely tedious but also necessary for this application (and seemed to be outside the scope of the project).

*This program with only index pages whose URL starts with the same URL as the original page.*  The default starting page is http://www.yahoo.com, you can change that with the -u parameter, but it won't go outside the original page domain (e.g. it would index http://www.yahoo.com/news but not http://www.bing.com).

##Searching / Ranking##

Searching is done by traversing the trie and creating a list of all the pages which contain every search term (separated by spaces) in the search query.

Ranking is done based on three simple parameters.  Number of times the word appears in the body of a document, in the title, and in the URL (each of these metrics carries a different weight).  This ranking is then multiplied by the page's *authority*.

###Authority##

Authority is a measure of how "trusted" a page is.  We calculate authority using the random surfer model.  In the simulation, a person picks a random page from the index and visits that page.  Then the person selects a random link from that page and follows the link, visiting the next page and so on.  At each jump, there's a 15% chance that the surfer will get bored and decide to jump to another page randomly (any page in the index).  

To calculate the authority of each page, a random surfer simulation is run periodically (it has to be re-run because authority changes as the graph changes).  The number of jumps in the simulation starts somewhat low.  The simulation is repeated if average change in page authority (from the previous authority) is above a certain threshold (2.5% in this case).  At each additional simulation, the number of jumps is increased.  Eventually the change is small enough to claim that we've reached convergence.

##Usage##

	python searchengine.py [-h | --help] [-m ... | --maxindex=] [-u ... | --url=] [-t ... | --threads=]
	-h:	Displays help message
	-m:	Max # of pages to index.
	-u:	Url to search first.  This is the first indexed page and only pages that start with this url are indexed. Defaults to https://www.yahoo.com
	-t: Threads to utilize in web crawler.

The search engine starts an HTTP server at http://localhost:8081.  You can use it to search the indexed pages with the **'search'** parameter.  For example, searching for the term 'yahoo':

	http://localhost:8081?search=yahoo

There is also an **allinfo** parameter which returns a list of all indexed pages and their rank (authority).

	http://localhost:8081?allinfo=true

##Files##

* **searchengine.py**  :  Entry point. Also contains ranking algorithm, and HTTP server.
* **WebSpider.py**  :  Multi-threaded web crawler.
* **kindex.py**  :  Page index.  Keeps a list of all the pages that have been indexed and runs the 'random surfer' simulation.  Contains indexing functions and a reference to the trie.  Also keeps information about pages that have been indexed such as id, title, URL, etc...
* **ktrie.py**  :  Compressed trie implementation.  Each node representing a string contains the IDs of all documents containing that string and the index of each occurrence of that string within the document.

*Unused*  

* **ksplay.py**  :  Splay tree implementation.  Thought it might be useful since we're performing searches, but it turns out that using hashtables within the trie are more efficient.
* **kbitree.py**  :  Binary tree implementation.  Similarly not used.

##Examples##

I choose to do some examples by crawling http://www.yahoo.com/celebrity for a few reasons:

1.  Celebrity pages seem to have a lot of linkage between them.
2.  Names are a pretty good search criteria.
3.  Typically the name of one celebrity will appear often in the articles about another.
4.  Document names are entertaining. :)

Start parameters (1000 indexed pages, starting from yahoo.com/celebrity, using 20 threads):

	python searchengine.py -m 3000 -u http://www.yahoo.com/celebrity -t 20

Getting all info (Sorts pages in order of authority, descending):

	http://localhost:8081?allinfo=true

	**All Page Info**

	ID	URL													Authority
	37	http://www.yahoo.com/celebrity/tagged/social-snaps	0.035378
	22	http://www.yahoo.com/celebrity/tagged/interviews	0.033151
	39	http://www.yahoo.com/celebrity/tagged/the-insider	0.032479
	32	http://www.yahoo.com/celebrity/tagged/photos		0.032185
	25	http://www.yahoo.com/celebrity/tagged/news			0.031639
	35	http://www.yahoo.com/celebrity/tagged/scandals		0.031639
	19	http://www.yahoo.com/celebrity/tagged/video			0.031387
	8	http://www.yahoo.com/celebrity/tagged/couples		0.031218
	...

Searching for "disney"

	http://localhost:8081?search=disney

	**Search Results:disney**

	0 http://www.yahoo.com/celebrity/former-disney-child-star-joey-084001869.html
	1 http://www.yahoo.com/celebrity/7-former-disney-stars-who-have-had-run-ins-with-131315742.html
	2 http://www.yahoo.com/celebrity/debby-ryan-disney-star-arrested-for-dui-213437139.html
	3 http://www.yahoo.com/celebrity/disney-star-kelli-berglund-arrested-coachella-170231760.html
	4 http://www.yahoo.com/celebrity/gwen-stefani-gets-a-blake-shelton-led-standing-165204018.html
	5 http://www.yahoo.com/celebrity/james-franco-celebrates-38th-birthday-050600497.html
	...

Searching for "prince"

	**Search Results:prince**

	0 http://www.yahoo.com/celebrity/arsenio-hall-sues-sinead-oconnor-over-prince-rant-195716585.html
	1 http://www.yahoo.com/celebrity/prince-william-in-his-own-words-i-am-a-prince-164645510.html
	2 http://www.yahoo.com/celebrity/wendy-williams-on-meeting-prince-he-was-a-025156132.html
	3 http://www.yahoo.com/celebrity/world-lights-purple-prince-155711045.html
	4 http://www.yahoo.com/celebrity/longtime-chef-prince-fought-throat-stomach-pains-recently-143522500.html
	5 http://www.yahoo.com/celebrity/8-wildest-celebrity-prince-encounters-184602003.html
	...

Searching for "maisie williams"

	**Search Results:maisie williams**

	0 http://www.yahoo.com/celebrity/instacram-maisie-williams-crashes-a-1415366258647094.html
	1 http://www.yahoo.com/celebrity/wendy-williams-on-meeting-prince-he-was-a-025156132.html
	2 http://www.yahoo.com/celebrity/police-katt-williams-charged-battery-georgia-155237507.html
	3 http://www.yahoo.com/celebrity/tagged/photos
	4 http://www.yahoo.com/celebrity/social-snaps-week-april-18-202232602.html
	5 http://www.yahoo.com/celebrity/tagged/video
	...

##Future Work##

* There's a much better way to calculate authority for a web page using eigen vectors.  Should implement that solution instead of the current random surfer simulation.
* Add additional ranking parameters, such as closeness of search terms and related terms search.
* Any real search engine needs to be represented in a database.  I had one implemented but it made indexing annoyingly slow.  Work for another  day.
* Search terms as vectors, relating them to other words?